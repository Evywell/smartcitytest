// Initialize app
var myApp = new Framework7({
    popupCloseByOutside: false,
    swipeBackPageActiveArea: 90,
    swipePanel: true,
    // Hide and show indicator during ajax requests
    onAjaxStart: function (xhr) {
        myApp.showIndicator();
    },
    onAjaxComplete: function (xhr) {
        myApp.hideIndicator();
    }
});

// If we need to use custom DOM library, let's save it to $$ variable:
var $$ = Dom7;

// Add view
var mainView = myApp.addView('.view-main', {
    // Because we want to use dynamic navbar, we need to enable it for this view:
    dynamicNavbar: true
});

// Identifiant de l'application à modifier pour chaque appli généré
const APP_ID = 1;

// URL du générateur de contenu
const LISTENER_URL = 'http://maville.digiplace.fr/Listener/';
var SQLite, sqlite;
var db_ok = false;
// Handle Cordova Device Ready Event
$$(document).on('deviceready', function() {
    
    console.log("Device is ready!");
    
    // Pour Android eviter à l'application de se fermer
    //document.addEventListener("backbutton", backKeyDown, true);
    $$(document).on("backbutton", function(e){
        e.preventDefault();
        mainView.router.back();
        
    });
    // On charge la base
    try {
        SQLite = window.cordova.require('cordova-sqlite-plugin.SQLite');
        sqlite = new SQLite('smartcity');
        
        sqlite.open(function(err) {
            if (err) throw err;
            
            sqlite.query('CREATE TABLE IF NOT EXISTS contenus(id VARCHAR(100) PRIMARY KEY, contenu LONGTEXT)',
            [], function(err, res){
                if (err) throw err;
                
                // Vérifier si connecté
                charger_panel();
                charger_contenus_distants('home').then(function(reponse){
                    // console.log(reponse);
                    
                    if(reponse === 'db_ok') {
                        charger_page('home', 0, false, function(){navigator.splashscreen.hide();}); 
                    }
                    else {
                        console.log("Le contenu en base n'est pas disponible");
                    }
                }).catch(function(err){
                    console.log(err);
                    
                    // On vérifie la presence de contenu local
                    
                    // Si contenu local on charge le panel et actualites
                    
                    // Si pas de contenu local on affiche une alerte de non disponibilité de contenu
                    navigator.splashscreen.hide();
                    myApp.alert("Assurez-vous d'être connecté au réseau et réessayer ultérieurement", 'Contenu indisponible');
                });

                charger_contenus_distants();
                
            });
            
        });
    }
    catch(e){
       console.log("Erreur initialisation sqlite");
       console.log(e); 
    }
    
    // Notification Push
    var push = PushNotification.init(
        {
            android: {
                senderID: "85075801930"
            },
            ios: {
                alert: "true",
                badge: "true",
                sound: "true"
            }
        }
    );
    
    push.on('registration', function(data){

        // On envoi sur le serveur
        var post_url = LISTENER_URL + '/register_push';
        var post_data = {
            "id_application": APP_ID,
            "token": data.registrationId,
            "topic": null,
            "os": device.platform
        }
        var success = function(data){
            console.log("Retour server succes registration");
            console.log(data);
        }

        var error = function(err) {
            console.log("Retour server erreur registration");
            console.log(err);
        }

        $$.post(post_url, post_data, success, error);
        // alert('registration id: ' + data.registrationId);
    });

    push.on('notification', function(data){
        console.log(data);
    });

    push.on('error', function(e){
        alert('erreur push: ' + e.message);
        console.log('err push');
        console.log(e);
    });

    
});

var xhr;
moment.updateLocale('fr', {calendar: {sameElse: 'll'}});
// Now we need to run the code that will be executed only for About page.

// Option 1. Using page callback for page (for "about" page in this case) (recommended way):
myApp.onPageInit('*', function(page) {
    // Do something here for "about" page
    console.log('init de ' + page.name);
        
    // Remplacer toutes les dates par leur equivalent moment.js
    temps_ecoule();
});

// FONCTIONS DE L'APP
function charger_panel() {

    try{
        sqlite.query('SELECT contenu FROM contenus WHERE id=?', ['panel'], 
            function(err, res){
                
                if(err) throw err;
                
                
                if(res.rows.length){
                    $$('.panel').html(res.rows[0].contenu);
                    console.log("panel local sqlite chargé");
                }
                else{
                    console.log("panel local absent");
                    console.log("tentative de recuperation du panel distant...");
                    charger_contenus_distants('panel').then(function(retour){

                        if(retour === 'db_ok'){
                            charger_panel();
                        }
                    }).catch(function(err){
                        alert('chargement distant panel echoué');
                    });
                }
            });

            setTimeout(charger_panel, 10000);
    }
    catch(e){
        console.log("Erreur de chargement du panel");
        console.log(e);
    }
}

function changer_couleur_navbar(le_scroll, la_reference) {

    // on ne le fait que lorsque la navbar est visible
    if (le_scroll > la_reference) {
        // console.log('Changement navbar')
        $$('.navbar').addClass('my-top-navbar-scrolling');
    } else {
        // console.log('reset navbar')
        $$('.navbar').removeClass('my-top-navbar-scrolling');
    }
}

// Fonction de récupération du contenu à afficher
function charger_page(page, id, anim, next) {

    var get_data = {
        application: APP_ID,
        page: page,
        id_actu: id || 0
    };
    
    var storage_index = get_data.page ;
    
    if( get_data.id_actu !== 0){
        storage_index += '_' + get_data.id_actu;
    }
    
    try {
        sqlite.query('SELECT * FROM contenus WHERE id=?', [storage_index], function(err, res){
        
        if(err) throw err;
        
        // console.log(res);

        if(res.rows.length){
            local_data = res.rows[0].contenu;
            my_router(local_data, anim, next);
            console.log("Contenu local sqlite chargé pour " + storage_index);
        }
        else{
            console.log("Aucun contenu trouvé en local pour " + storage_index);
            myApp.alert("Assurez-vous d'être connecté au réseau et réessayer ultérieurement", 'Contenu indisponible');
        }
        });

        
     }
     catch(e){
        console.log('Erreur appel sqlite');
        console.log(e.message);
     }
    
}

// Function de chargement du contenu
function my_router(data, anim, next){
    if (typeof next !== 'undefined') {
        mainView.router.load({ content: data, animatePages: false });
        next();
    } else {
        mainView.router.load({ content: data, animatePages: anim });
    }
}

// Gestion personnalisée des clics pour soit charger une page ou déclencher une action perso
function gestion_click(elt) {

    var $target = $$(event.target);

    // console.log('target class: ' + $target.html());

    if ($target.html() === 'more_vert') {
        console.log('clic sur options detecté');

        var titre = $$('.my-card-title').html();
        var img = $$('.card-content').find('img').attr('src');
        var content = $$('.card-content-text').text();
        
    
        if(titre === undefined){ // Agenda
            titre = $$('.my-subheader-titre').html();
        }
    
        if(titre.indexOf('</div>') != -1){ // Titre
            titre = titre.split('</div>');
            titre = titre[1];
        }
        
        if(content === null){
            content = $$('.card-content-inner').text(); // Contenu dynamique pour les génériques
            if(content === null){
                content = titre;
            }
        }
        
        // this is the complete list of currently supported params you can pass to the plugin (all optional)
        var options = {
          message: content, // not supported on some apps (Facebook, Instagram)
          subject: titre, // fi. for email
          files: [img], // an array of filenames either locally or remotely
          url: '',
          chooserTitle: 'Choisissez une application' // Android only, you can override the default share sheet title
        }

        var onSuccess = function(result) {
          console.log("Partage terminé ? " + result.completed); // On Android apps mostly return false even while it's true
          console.log("Partagé avec l'application : " + result.app); // On Android result.app is currently empty. On iOS it's empty when sharing is cancelled (result.completed=false)
        }

        var onError = function(msg) {
          console.log("Partagé raté avec comme erreur : " + msg);
        }

        window.plugins.socialsharing.shareWithOptions(options, onSuccess, onError);

    } else {
        var page = $$(elt).data('my-page');
        var id_actu = $$(elt).data('my-id-actu');

        console.log('appel de de charger page avec page = ' + page + ' et id_actu = ' + id_actu);

        // On gère la fermeture du panel pour les liens depuis le menu
        if ($$(elt).hasClass('my-panel-blocs')) {
            console.log('Chargement depuis le panel');
            var next = function() { myApp.closePanel(); }
            charger_page(page, id_actu, true, next);
        } else {
            charger_page(page, id_actu);
        }
    }
}

// Fonction d'envoi d'email
function mail(to, subject, body) {

    console.log('appel de la fonction mail');

    alert('inside mail');

    cordova.plugins.email.isAvailable(
        function(hasAccount) {

            alert("is email mobile available ? " + (hasAccount ? "Yes" : "No"));

            if (hasAccount) {
                cordova.plugins.email.open({
                    to: '',
                    subject: '',
                    body: ''
                });
            }

        });
}

// Fonction Ouverture popup (l'element doit être présent dans la page)
function openMyPop(pop) {
    myApp.popup(pop);
}

function closeMyPop(pop) {
    // alert('fermeture du modal demandée');
    myApp.closeModal(pop);
}

// === GESTION PHOTO SIGNALEMENTS === //
// Défini les options des photos prises
function setOptions(srcType) {
    var options = {
        // Some common settings are 20, 50, and 100
        quality: 50,
        destinationType: Camera.DestinationType.FILE_URI,
        // In this app, dynamically set the picture source, Camera or photo gallery
        sourceType: srcType,
        encodingType: Camera.EncodingType.JPEG,
        mediaType: Camera.MediaType.PICTURE,
        allowEdit: true,
        correctOrientation: true //Corrects Android orientation quirks
    }
    return options;
}
// Fonction de prise de photo
function openCamera(selection) {

    // alert('appel de la camera avec selection : ' + selection);
    if (selection === 'prendre') {
        var srcType = Camera.PictureSourceType.CAMERA;
    } else {
        var srcType = Camera.PictureSourceType.PHOTOLIBRARY;
    }

    var options = setOptions(srcType);
    //var func = createNewFileEntry;

    navigator.camera.getPicture(function cameraSuccess(imageUri) {

        displayImage(imageUri);
        // You may choose to copy the picture, save it somewhere, or upload.
        //func(imageUri);
        openMyPop('.popup-photo');

    }, function cameraError(error) {
        console.debug("Unable to obtain picture: " + error, "app");

    }, options);
}

// Affichage de la photo prise
function displayImage(imgUri) {
    var elem = document.getElementById('photo-selectionnee');
    var elem2 = document.getElementById('imageUri');

    elem.src = imgUri; // Affiche la photo
    elem2.value = imgUri;
}

// Envoi les données de signalement sur le serveur distant
function envoi_signalement() {
    console.log('entree dans envoi signalement');
    var formData = myApp.formToData('#my-signalement-form');

    console.log(formData);
    // alert(JSON.stringify(formData));

    formData.id_application = APP_ID;
    // console.log('envoi distant...');
    imageURI = document.getElementById('photo-selectionnee').getAttribute("src");

    if (!imageURI) {
       alert('Please select an image first.');
        // return;
        console.log('Aucune image fournie');
    }
    

    // alert(imageURI);

    //Envoi du fichier avec le plugin
    var succes = function(r) {
        console.log("Code = " + r.responseCode);
        console.log("Response = " + r.response);
        console.log("Sent = " + r.bytesSent);

        // On ferme le modal
        closeMyPop('.popup-photo');

        // On affiche la page de remerciement
        openMyPop('.popup-merci');
    };

    var echec = function(error) {
        alert("Une erreur est survenue : Code = " + error.code);
        console.log("Upload error source " + error.source);
        console.log("upload error target " + error.target);
    };

    var options = new FileUploadOptions();
    options.fileKey = "file";
    options.fileName = imageURI.substr(imageURI.lastIndexOf('/') + 1);
    options.mimeType = "image/jpeg";
    console.log(options.fileName);
    options.params = formData;
    console.log("les params");
    console.log(options.params);

    var ft = new FileTransfer();
    ft.upload(imageURI, encodeURI(LISTENER_URL + '/upload/'), succes, echec, options);
}

// fonction de traitement des sondages
function envoi_sondage(formulaire){
    console.log('envoi du sondage ' + formulaire);
    var formData = myApp.formToData(formulaire);

    console.log(formData);
    
    var post_data = formData;
    $$.post(LISTENER_URL + '/sondage/', post_data, function(reponse){
        
        console.log('retour envoi sondage');
        console.log(reponse);
        console.log(reponse.error);

        reponse = JSON.parse(reponse);

        console.log(reponse);
        console.log(reponse.error);

        if( typeof reponse.error !== 'undefined'){
            myApp.alert(reponse.error, "Participation incomplète");
        }
        else {
            myApp.alert(reponse.succes, "Participation validée", function(){
                mainView.router.back();
            });
        }
    });
}

function charger_contenus_distants(id){
    
    var fetch_url = LISTENER_URL + '/all_data/' + APP_ID;
    var id_present = typeof id !== 'undefined';
    
    if( id_present ){
        fetch_url += '/' + id;
        console.log("debut de la mise à jour depuis le serveur distant: " + id);
    }
    else{
        console.log("debut de la mise à jour depuis le serveur distant");
    }
    
    try{
        return window.fetch(fetch_url).then(
            function(reponse){
                return reponse.json();
            }).then(
                function(data){
                    return Promise.all(
                            data.map(
                                function(page){
                                    sqlite.query('REPLACE INTO contenus(id, contenu) VALUES(?,?)', 
                                        [page._id ,page.contenu],
                                        function(err, res){
                                            if(err) throw err;
                                            // console.log("insertion Ok pour " + page._id);
                                        }
                                    );
                                }
                            )
                        );
        }).then(function(){

            console.log("Fin de la mise à jour distante" + (id_present ? ': ' + id : '') );
            
            // On ne rappelle que les demandes générales
            if( ! id_present ){
                setTimeout(charger_contenus_distants, 60000);
            }
            
            return "db_ok";
        }).catch(function(err){
            console.log("Une erreur est survenue lors de la mise à jour des données");
            console.log(err.message);
        });
    }
    catch(e){
        console.log(e);
    }
    
}

// Affichage propre de date
function temps_ecoule(){
    console.log("Mise à jour des dates de publication");
    $$(".moment-times").each(function(){
        var myDate = $$(this).text();
        var myMoment = moment(myDate, "YYYY-MM-DD hh:mm:ss").calendar();
        $$(this).parent('.my-card-date').find('.moment-times-target').text(myMoment);
    });
    
    setTimeout(temps_ecoule, 120000); // toutes les 2 minutes
}

// Gestion affichage bouton back
var nb_pages = 0;

myApp.onPageBeforeInit('*', function(page) {
    // Do something here for "about" page
    console.log('BeforeInit de ' + page.name);
    nb_pages++;
    console.log('nb_pages: ' + nb_pages);
    show_hide_back_button();
    
});

myApp.onPageBeforeRemove('*', function(page) {
    // Do something here for "about" page
    console.log('BeforeRemove de ' + page.name);
    nb_pages--;
    console.log('nb_pages: ' + nb_pages);
    show_hide_back_button();
    
});

// détermine si c'est nécessaire d'afficher un bouton de retour sur la navbar
function show_hide_back_button(){
    
    if(nb_pages > 1) {
        // console.log("historique disponible");
        $$('.navbar .back').show();
    }
    else {
        // console.log("pas d'historique");
        $$('.navbar .back').hide();
    }
}

function charger_contenu_local(page, id) {

    var promise = new Promise(function(resolve, reject){
        sqlite.query('SELECT contenu FROM contenus WHERE id=?', [page], function(err, res){

            if( err || ! res.rows.length){
                reject(err);
            }

            if( res.rows.length ){
                resolve(res.rows);
            }
            
        });
    });

    return promise;
}

function charger_contenu_distant(page, id){

    var url = LISTENER_URL + '/all_data/' + APP_ID + '/' + page;

    if( typeof id !== 'undefined' && id > 0) {
        url += '/' + id;
    }

    var promise = new Promise(function(resolve, reject){

        $$.getJSON(url, function(data){ resolve(data); }, function(err){ reject(err); });
    });

    return promise;
}

function charger_page_local_distant(page, id){

    charger_contenu_local(page, id)
    .then(function(contenu){
        my_router(contenu)
    })
    .catch(function(err){
        charger_contenu_distant(page,id)
        .then(function(contenu){
            my_router(contenu)
        })
        .catch(function(err){
            console.log("Impossible de récupérer le contenu " + page );
        });
    });
}